import { Link } from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown';
import Button from 'react-bootstrap/Button';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav align-items-center">

          <li className="nav-item mx-auto">
          <Button className="btn btn-success fw-bold" as={Link} to="/">CarCar</Button>
          </li>
          <li className="nav-item mx-auto">
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              Inventory
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item as={Link} to="/manufacturers">Vehicle Manufacturers</Dropdown.Item>
              <Dropdown.Item as={Link} to="/manufacturers/create">Create a Manufacturer</Dropdown.Item>
              <Dropdown.Item as={Link} to="/models">Vehicle Models</Dropdown.Item>
              <Dropdown.Item as={Link} to="/models/create">Create a Model</Dropdown.Item>
              <Dropdown.Item as={Link} to="/automobiles">Automobile Inventory</Dropdown.Item>
              <Dropdown.Item as={Link} to="/automobiles/create">Create an Automobile</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          </li>
          <li className="nav-item mx-auto">
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              Service
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item as={Link} to="/technicians">Registered Technicians</Dropdown.Item>
              <Dropdown.Item as={Link} to="/technicians/create">Register a Technician</Dropdown.Item>
              <Dropdown.Item as={Link} to="/appointments">Scheduled Appointments</Dropdown.Item>
              <Dropdown.Item as={Link} to="/appointments/create">Schedule an Appointment</Dropdown.Item>
              <Dropdown.Item as={Link} to="/appointments/history">Appointment History</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          </li>
          <li className="nav-item mx-auto px-auto">
          <Dropdown>
            <Dropdown.Toggle variant="success" id="dropdown-basic">
              Sales
            </Dropdown.Toggle>
            <Dropdown.Menu>
              <Dropdown.Item as={Link} to="/salespeople">Sales Employees</Dropdown.Item>
              <Dropdown.Item as={Link} to="/salespeople/create">Add an Employee</Dropdown.Item>
              <Dropdown.Item as={Link} to="/customers">List Customers</Dropdown.Item>
              <Dropdown.Item as={Link} to="/customers/create">Add a Customer</Dropdown.Item>
              <Dropdown.Item as={Link} to="/sales">Vehicle Sales</Dropdown.Item>
              <Dropdown.Item as={Link} to="/sales/create">Add a Sale</Dropdown.Item>
              <Dropdown.Item as={Link} to="/sales/history">Sales History</Dropdown.Item>
            </Dropdown.Menu>
          </Dropdown>
          </li>

          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
