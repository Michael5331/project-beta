import { useEffect, useState } from "react";

function AutomobilesList() {

    const [autos, setAutos] = useState([]);

    async function loadAutomobiles() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const data = await response.json();
            setAutos(data.autos)
        }
    }

    useEffect(() => {
        loadAutomobiles();
    }, []);

    return(
        <div className='container my-4'>
            <h1 className='display-5 fw-bold'>Automobiles</h1>
            <table className='table table-striped'>
                <thead>
                    <tr className='bg-success'>
                        <th className='text-white text-center'>VIN</th>
                        <th className='text-white text-center'>Color</th>
                        <th className='text-white text-center'>Year</th>
                        <th className='text-white text-center'>Model</th>
                        <th className='text-white text-center'>Manufacturer</th>
                        <th className='text-white text-center'>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos && autos.map(auto => {
                        let sold = 'No';
                        if (auto.sold) {sold = 'Yes'};
                        return (
                            <tr key={auto.href} value={auto.href}>
                                <td className='table-success text-center fw-bold'>{auto.vin}</td>
                                <td className='table-success text-center fw-bold'>{auto.color}</td>
                                <td className='table-success text-center fw-bold'>{auto.year}</td>
                                <td className='table-success text-center fw-bold'>{auto.model.name}</td>
                                <td className='table-success text-center fw-bold'>{auto.model.manufacturer.name}</td>
                                <td className='table-success text-center fw-bold'>{sold}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );

}
export default AutomobilesList;
