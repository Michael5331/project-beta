import React, {useState, useEffect} from 'react'

function SalesForm() {

    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [price, setPrice] = useState('');
    const [automobiles, setAutomobiles]  = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [hasSignedUp, setHasSignedUp] = useState(false);

    const fetchDataAutomobiles = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchDataAutomobiles();
    }, []);

    const fetchDataCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data.customers);
        }
    }

    useEffect(() => {
        fetchDataCustomers();
    }, []);

    const fetchDataSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople);
        }
    }

    useEffect(() => {
        fetchDataSalespeople()
    }, []);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer.slice(-2, -1);
        data.price = price;

        const salesUrl = 'http://localhost:8090/api/sales/';
        console.log(data)
        const fetchOptions = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const salesResponse = await fetch(salesUrl, fetchOptions);
        if (salesResponse.ok) {
            setAutomobile('');
            setSalesperson('');
            setCustomer('');
            setPrice('');
            setHasSignedUp(true);
        }
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }

    const handleSalespersonChange= (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (customers.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select border-success';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = ''
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }

    return(
        <div className='my-5 container'>
            <div className='row'>
                <div className='col'></div>
                <div className='col'>
                    <div className='card shadow'>
                        <div className='card-body'>

                            <form className={formClasses} onSubmit={handleSubmit} id='create-sales-form'>
                                <h1 className='card-title'>Record a new sale</h1>

                                <div className='mb-3'>
                                    <label className='fw-bold' htmlFor='automobile'>Automobile</label>

                                    <div className={spinnerClasses} id='loading-automobile-spinner'>
                                        <div className='spinner-grow text-success' role='status'>
                                            <span className='visually-hidden'>Loading...</span>
                                        </div>
                                    </div>
                                    <select onChange={handleAutomobileChange} name='automobile' id='automobile' className={dropdownClasses} required>
                                        <option value=''>Choose an automobile...</option>
                                        {automobiles.map(automobile => {
                                            return (
                                                <option key={automobile.href} value={automobile.vin}>{automobile.vin}</option>
                                            )
                                        })}
                                    </select>
                                </div>

                                <div className='mb-3'>
                                    <label className='fw-bold' htmlFor='salesperson'>Salesperson</label>
                                    <div className={spinnerClasses} id='loading-salesperson-spinner'>
                                        <div className='spinner-grow text-success' role='status'>
                                            <span className='visually-hidden'>Loading...</span>
                                        </div>
                                    </div>
                                    <select onChange={handleSalespersonChange} name='salesperson' id='salesperson' className={dropdownClasses} required>
                                        <option value=''>Choose a salesperson...</option>
                                        {salespeople.map(salesperson => {
                                            return (
                                                <option key={salesperson.href} value={salesperson.employee_id}>{salesperson.first_name} {salesperson.last_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>

                                <div className='mb-3'>
                                    <label className='fw-bold' htmlFor='customer'>Customer</label>
                                    <div className={spinnerClasses} id='loading-customer-spinner'>
                                        <div className='spinner-grow text-success' role='status'>
                                            <span className='visually-hidden'>Loading...</span>
                                        </div>
                                    </div>
                                    <select onChange={handleCustomerChange} name='customer' id='customer' className={dropdownClasses} required>
                                        <option value=''>Choose a customer...</option>
                                        {customers.map(customer => {
                                            return (
                                                <option key={customer.href} value={customer.href}>{customer.first_name} {customer.last_name}</option>
                                            )
                                        })}
                                    </select>
                                </div>

                                <div className='col'>
                                    <label className='fw-bold' htmlFor='price'>Price</label>
                                        <div className='form-floating mb-3'>
                                            <input onChange={handlePriceChange} required placeholder='Price' type='text' id='price' name='price' className='form-control' />
                                        </div>
                                </div>

                                <button className='btn btn-lg btn-success'>Save Sale</button>
                            </form>
                            <div className={messageClasses} id='success-message'>
                                Sale saved successfully! If you would like to record another sale <a href='/sales/create' className='alert-link'>click here. </a>
                                To see the sales list <a href='/sales' className='alert-link'>click here.</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className='col'></div>
            </div>
        </div>
    );

}
export default SalesForm;
